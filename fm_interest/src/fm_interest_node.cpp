#include "ros/ros.h"
#include <stdlib.h>
#include <math.h>
#include "nav_msgs/OccupancyGrid.h"
#include <vector>
#include <unistd.h>
#include <queue>
#include <fstream>
#include <iostream>
#include <stdio.h>


class SubscribeAndPublish
{
public:

    struct AdyList
    {
        std::vector <int> parent;
        std::vector <int> child;
    };

    struct ProbList
    {
        std::vector <double> prob;
    };

    std::string path;
    std::string prefix;
    int secs;
    bool print;

    std::vector <double> OccGrid_im;
    std::vector <int> labelMap;
    std::vector <signed char> im_char;
    double bins;
    double min_area;
    int pedestrian;

  SubscribeAndPublish()
  {
    std::string map_fm_topic;
    std::string map_labelled_topic;

    ros::NodeHandle nhParam("~");

    nhParam.param("Map_Fm_topic", map_fm_topic, std::string("/map_fm"));
    nhParam.param("Map_labelled_topic", map_labelled_topic, std::string("/map_labelled"));
    nhParam.param("bins", bins, 20.0);
    nhParam.param("min_area", min_area, 1.0);
    nhParam.param("path", path, std::string("/home/dagarzonr/Dropbox/Papers/2016_robocity/graphs"));
    nhParam.param("prefix", prefix, std::string("graph"));
    nhParam.param("print", print, false);


    //Topic you want to publish    
    im_pub2 = nodeHandle.advertise<nav_msgs::OccupancyGrid>(map_labelled_topic, 1);

    //Topic you want to subscribe
    sub1 = nodeHandle.subscribe(map_fm_topic, 1, &SubscribeAndPublish::mapCallback, this);
  }

  void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& map_msg)
  {
      secs = map_msg->header.stamp.sec;

      ROS_INFO("Filas y Columnas:  [%d,%d]", map_msg->info.height,map_msg->info.width);
      int size_of = map_msg->info.height*map_msg->info.width;
      OccGrid_im.resize(size_of);

      for (int a=0; a<size_of; a++)
      {
          OccGrid_im[a] = (int)map_msg->data[a];
      }  

      if (OccGrid_im.size()!=0)
      {

          /*-------- Second Max Value ----------*/

          int OccGrid_im_size = OccGrid_im.size();
          double OccGrid_im_max = 0;

          for (int w=0; w<OccGrid_im_size; w++)
          {
              if (OccGrid_im[w] < 100 && OccGrid_im[w] > OccGrid_im_max)
                  {
                      OccGrid_im_max = OccGrid_im[w];
                  }
          }

          /*-------- the chicha  ----------*/

          AdyList adyacences = conectedComponentLabelling(map_msg->info.width,OccGrid_im_max);

          printIntegerMap("map_areas", labelMap, map_msg->info.width);
          printGraph("graph_areas", adyacences);

          ProbList probability = areaProbability(adyacences, min_area, map_msg->info.resolution, pedestrian);

          printProbList("prob_areas", probability);

          /*-------- Publish  Prob ----------*/

          nav_msgs::OccupancyGrid map_labelled;

          map_labelled.header = map_msg->header;
          map_labelled.info = map_msg->info;

          im_char.resize(OccGrid_im_size);

          for (int z=0; z<OccGrid_im_size; z++)
          {
              if (labelMap[z] == 0)
              {
                  im_char[z]=(signed char)100;
              }              
              else
              {
                  im_char[z]=(signed char)ceil(99*(1-(probability.prob[labelMap[z]]/100)));
              }
          }

          std::vector <int> prob_map;
          prob_map.resize(labelMap.size());
          for (int i=0; i<OccGrid_im_size; i++)
          {
              if (labelMap[i] == 0)
              {
                  prob_map[i]= 0;
              }
              else
              {
                  prob_map[i]=ceil(100*(probability.prob[labelMap[i]]/100));
              }
          }

          printIntegerMap("map_prob", prob_map, map_msg->info.width);

          map_labelled.data = im_char;

          im_pub2.publish(map_labelled);

          im_char.clear();
          labelMap.clear();
          OccGrid_im.clear();
          prob_map.clear();

      return;
    }
  }

  AdyList conectedComponentLabelling(int map_width, double OccGrid_im_max)
  {
      int pixel_up;
      int pixel_left;
      int restrict_l;

      int OccGrid_im_size = OccGrid_im.size();

      double n_bin = ceil(bins);
      double s_bin = OccGrid_im_max/n_bin;

      labelMap.resize(OccGrid_im.size());      

      int bin_p = 0;
      int bin_pu = 0;
      int bin_pl = 0;

      int label_pu = 0;
      int label_pl = 0;
      int label_count = 0;
      AdyList ady;

      for (int pixel=0; pixel<OccGrid_im_size; pixel++)
      {

          if (OccGrid_im[pixel] <100)
          {
              pixel_up = pixel - map_width;
              pixel_left = pixel - 1;
              restrict_l = pixel % map_width;

              /* Security and Histogram Conditions */

              if (OccGrid_im[pixel] == 0)
              {
                  bin_p = 0;
                  pedestrian = pixel;
              }
              else
              {
                  bin_p = ceil(OccGrid_im[pixel] / s_bin)-1;
              }

              if (pixel == 0)
              {
                  label_pu = 0;
                  label_pl = 0;
              }
              else if (pixel < map_width)
              {
                  label_pu = 0;

                  if (OccGrid_im[pixel_left] == 100)
                  {
                      label_pl = 0;
                  }
                  else
                  {
                      if (OccGrid_im[pixel_left] == 0)
                      {
                          bin_pl = 0;
                      }
                      else
                      {
                          bin_pl = ceil(OccGrid_im[pixel_left] / s_bin)-1;
                      }

                      if (bin_p == bin_pl)
                      {
                          label_pl = labelMap[pixel_left];
                      }
                      else
                      {
                          label_pl = 0;
                      }
                  }
              }
              else if (restrict_l == 0)
              {
                  label_pl = 0;

                  if (OccGrid_im[pixel_up] == 100)
                  {
                      label_pu = 0;
                  }
                  else
                  {
                      if (OccGrid_im[pixel_up] == 0)
                      {
                          bin_pu = 0;
                      }
                      else
                      {
                          bin_pu = ceil(OccGrid_im[pixel_up] / s_bin)-1;
                      }

                      if (bin_p == bin_pu)
                      {
                          label_pu = labelMap[pixel_up];
                      }
                      else
                      {
                          label_pu = 0;
                      }
                  }
              }
              else
              {
                  if (OccGrid_im[pixel_up] == 100)
                  {
                      label_pu = 0;
                  }
                  else
                  {
                      if (OccGrid_im[pixel_up] == 0)
                      {
                          bin_pu = 0;
                      }
                      else
                      {
                          bin_pu = ceil(OccGrid_im[pixel_up] / s_bin)-1;
                      }

                      if (bin_p == bin_pu)
                      {
                          label_pu = labelMap[pixel_up];
                      }
                      else
                      {
                          label_pu = 0;
                      }
                  }

                  if (OccGrid_im[pixel_left] == 100)
                  {
                      label_pl = 0;
                  }
                  else
                  {
                      if (OccGrid_im[pixel_left] == 0)
                      {
                          bin_pl = 0;
                      }
                      else
                      {
                          bin_pl = ceil(OccGrid_im[pixel_left] / s_bin)-1;
                      }
                      if (bin_p == bin_pl)
                      {
                          label_pl = labelMap[pixel_left];
                      }
                      else
                      {
                          label_pl = 0;
                      }
                  }
              }

              /* Labelling */

              if (label_pu == 0 && label_pl == 0)
              {
                  label_count = label_count + 1;
                  labelMap[pixel] = label_count;                  
              }
              else if (label_pu == 0 && label_pl != 0)
              {
                  labelMap[pixel] = label_pl;
              }
              else if (label_pl == 0 && label_pu != 0)
              {
                  labelMap[pixel] = label_pu;
              }
              else if (label_pu == label_pl)
              {
                  labelMap[pixel] = label_pu;
              }
              else if (label_pu > label_pl)
              {
                  labelMap[pixel] = label_pl;
                  ady.parent.resize(ady.parent.size()+1);
                  ady.child.resize(ady.child.size()+1);
                  ady.parent[ady.parent.size()-1] = label_pl;
                  ady.child[ady.child.size()-1] = label_pu;                  
              }
              else if (label_pu < label_pl)
              {
                  labelMap[pixel] = label_pu;
                  ady.parent.resize(ady.parent.size()+1);
                  ady.child.resize(ady.child.size()+1);
                  ady.parent[ady.parent.size()-1] = label_pu;
                  ady.child[ady.child.size()-1] = label_pl;                  
              }
              else
              {
                  ROS_INFO("WTF!!!!!");
              }

          }
          else
          {
              labelMap[pixel] = 0;
              label_pu = 0;
              label_pl = 0;
          }
      }

      /* Cleaning the Conection List */

      int parent = 0;
      int child = 0;

      int lab_p_size = ady.parent.size();

      for (int j=0; j<lab_p_size; j++)
      {
          if (j<ady.parent.size())
          {
              parent = ady.parent[j];
              child = ady.child[j];
              int conection_size = ady.parent.size();

              for (int k = conection_size-1; k>j; k--)
              {
                  if (parent == ady.parent[k] && child == ady.child[k])
                  {
                      ady.parent.erase(ady.parent.begin()+k);
                      ady.child.erase(ady.child.begin()+k);
                  }
                  else if (parent == ady.child[k] && child == ady.parent[k])
                  {
                      ady.parent.erase(ady.parent.begin()+k);
                      ady.child.erase(ady.child.begin()+k);
                  }
              }
          }
      }      

      double label_max = *std::max_element(ady.child.begin(),ady.child.end());
      int label_max_ = (int)label_max;

      std::vector <int> replace_list;
      double lm_max = *std::max_element(labelMap.begin(),labelMap.end());
      int lm_max_ = (int)lm_max;
      replace_list.resize(lm_max_+1);

      for (int i=0; i < replace_list.size(); i++)
      {
          replace_list[i] = i;
      }

      std::vector <int> equal_parents;
      int equal_size = 0;

      for (int sub_ind=label_max_; sub_ind > 0; sub_ind--)
      {          
          equal_parents.resize(equal_parents.size()+1);
          equal_parents[equal_parents.size()-1] = sub_ind;

          for (int n=0; n<ady.parent.size(); n++)
          {
              if (ady.child[n] == sub_ind)
              {
                  equal_parents.resize(equal_parents.size()+1);
                  equal_parents[equal_parents.size()-1] = ady.parent[n];
              }
              if (ady.parent[n] == sub_ind)
              {
                  equal_parents.resize(equal_parents.size()+1);
                  equal_parents[equal_parents.size()-1] = ady.child[n];
              }
          }

          equal_size = equal_parents.size();

          if (equal_size>=1)
          {
              double equal_min = *std::min_element(equal_parents.begin(),equal_parents.end());

              int equal_min_ = (int)equal_min;

              for (int eq=0; eq<equal_size; eq++)
              {
                  std::replace (replace_list.begin(), replace_list.end(), equal_parents[eq], equal_min_);
                  std::replace (ady.parent.begin(), ady.parent.end(), equal_parents[eq], equal_min_);
                  std::replace (ady.child.begin(), ady.child.end(), equal_parents[eq], equal_min_);
              }

              equal_parents.clear();
          }

          equal_size = 0;
      }     

      int replace_tmp = 0;
      for (int i=0; i<labelMap.size(); i++)
      {
          replace_tmp = replace_list[labelMap[i]];          
          labelMap[i] = replace_tmp;
      }

      replace_list.clear();
      replace_tmp = 0;

      /* Conecting Areas */

      label_pu = 0;
      label_pl = 0;
      ady.parent.clear();
      ady.child.clear();
      int label_map_size = labelMap.size();

      for (int pix_l=0; pix_l<label_map_size; pix_l++)
      {
          if (labelMap[pix_l] != 0)
          {
              pixel_up = pix_l - map_width;
              pixel_left = pix_l - 1;
              restrict_l = pix_l % map_width;

              /* Security Conditions */

              if (pix_l == 0)
              {
                  label_pu = 0;
                  label_pl = 0;
              }
              else if (pix_l < map_width)
              {
                  label_pu = 0;
                  label_pl = labelMap[pixel_left];
              }
              else if (restrict_l == 0)
              {
                  label_pl = 0;
                  label_pu = labelMap[pixel_up];
              }
              else
              {
                  label_pu = labelMap[pixel_up];
                  label_pl = labelMap[pixel_left];
              }

              /* Conecting Labels - Can detect de frontiers */

              if (label_pu != 0 && label_pu != labelMap[pix_l])
              {
                  ady.parent.resize(ady.parent.size()+1);
                  ady.child.resize(ady.child.size()+1);
                  ady.parent[ady.parent.size()-1] = labelMap[pix_l];
                  ady.child[ady.child.size()-1] = labelMap[pixel_up];
              }

              if (label_pl != 0 && label_pl != labelMap[pix_l])
              {
                  ady.parent.resize(ady.parent.size()+1);
                  ady.child.resize(ady.child.size()+1);
                  ady.parent[ady.parent.size()-1] = labelMap[pix_l];
                  ady.child[ady.child.size()-1] = labelMap[pixel_left];
              }
          }
      }    

      /* Cleaning the Conection List */

      parent = 0;
      child = 0;
      lab_p_size = ady.parent.size();

      for (int j=0; j<lab_p_size; j++)
      {
          if (j<ady.parent.size())
          {
              parent = ady.parent[j];
              child = ady.child[j];
              int conection_size = ady.parent.size();

              for (int k = conection_size-1; k>j; k--)
              {
                  if (parent == ady.parent[k] && child == ady.child[k])
                  {
                      ady.parent.erase(ady.parent.begin()+k);
                      ady.child.erase(ady.child.begin()+k);
                  }
                  else if (parent == ady.child[k] && child == ady.parent[k])
                  {
                      ady.parent.erase(ady.parent.begin()+k);
                      ady.child.erase(ady.child.begin()+k);
                  }
              }
          }
      }      

      /* Re-label with ordered values */

      std::vector <int> ord_values;

      ord_values.insert(ord_values.end(),ady.parent.begin(),ady.parent.end());
      ord_values.insert(ord_values.end(),ady.child.begin(),ady.child.end());
      sort(ord_values.begin(),ord_values.end());
      ord_values.erase(unique(ord_values.begin(),ord_values.end()),ord_values.end());

      int relabel_size = ord_values.size();

      double ordv_max = *std::max_element(ord_values.begin(),ord_values.end());
      int ordv_max_ = (int)ordv_max;

      std::vector <int> ord_list;
      ord_list.resize(ordv_max_+1);

      for (int i=0; i < ord_list.size(); i++)
      {
          ord_list[i] = i;
      }

      for (int p=1; p<=relabel_size; p++)
      {                    
          ord_list[ord_values[p-1]] = p;
          //std::replace (labelMap.begin(), labelMap.end(), ord_values[p-1], p);
          std::replace (ady.parent.begin(), ady.parent.end(), ord_values[p-1], p);
          std::replace (ady.child.begin(), ady.child.end(), ord_values[p-1], p);          
      }

      int ord_tmp = 0;
      for (int i=0; i<labelMap.size(); i++)
      {
          ord_tmp = ord_list[labelMap[i]];
          labelMap[i] = ord_tmp;
      }

      ord_list.clear();

      ord_values.clear();

      //printIntegerMap("lm_areas_order", labelMap, map_width);

      //printGraph("graph_areas_order", ady);

      /* Return */

      return ady;
  }

  ProbList areaProbability (AdyList conections, double area_min, double map_resolution, int pedestrian)
  {
      /* Histogram */
      std::vector <int> histogram;
      int hist_tmp=0;

      int map_size = labelMap.size();
      double map_max = *std::max_element(&labelMap[0],&labelMap[0]+map_size);
      int map_max_ = (int)map_max;
      histogram.resize(map_max_+1);

      for (int i=0; i<map_size; i++)
      {
          hist_tmp = histogram[labelMap[i]];
          histogram[labelMap[i]] = hist_tmp+1;
      }

      /* Probabilty Map*/

      int ped_area = labelMap[pedestrian];
      ROS_INFO("Pedestrian = %d",ped_area);

      int pixel_min = pow(ceil(area_min/map_resolution),2);
      int front = 0;
      double num_conect = 0.0;
      double local_prob = 100.0;
      double past_prob;

      std::queue <int> ady_tree;
      std::vector <int> local_list;
      std::vector <int> erase_list;
      std::vector <double> prob_tree;

      prob_tree.resize(map_max_+1);

      ady_tree.push(ped_area);
      prob_tree[ped_area] = 100.0;

      while (!ady_tree.empty())
      {
          front = ady_tree.front();
          for (int i=0; i<conections.parent.size(); i++)
          {
              if (conections.parent[i]==front)
              {
                  if (histogram[conections.child[i]]<pixel_min)
                  {
                      prob_tree[conections.child[i]] = prob_tree[front];
                  }
                  else
                  {
                      local_list.resize(local_list.size()+1);
                      local_list[local_list.size()-1] = conections.child[i];
                  }
                  ady_tree.push(conections.child[i]);
                  erase_list.resize(erase_list.size()+1);
                  erase_list[erase_list.size()-1] = i;
              }
              else if (conections.child[i]==front)
              {
                  if (histogram[conections.parent[i]]<pixel_min)
                  {
                      prob_tree[conections.parent[i]] = prob_tree[front];
                  }
                  else
                  {
                      local_list.resize(local_list.size()+1);
                      local_list[local_list.size()-1] = conections.parent[i];
                  }
                  ady_tree.push(conections.parent[i]);
                  erase_list.resize(erase_list.size()+1);
                  erase_list[erase_list.size()-1] = i;
              }
          }

          num_conect = (double)local_list.size();
          local_prob = prob_tree[front]/num_conect;

          for (int i=0; i<num_conect; i++)
          {
              past_prob = prob_tree[local_list[i]];
              prob_tree[local_list[i]] = past_prob + local_prob;
          }

          sort(erase_list.begin(),erase_list.end());
          int erase_list_size = erase_list.size();

          for (int i=erase_list_size-1; i>=0; i--)
          {
              conections.parent.erase(conections.parent.begin()+erase_list[i]);
              conections.child.erase(conections.child.begin()+erase_list[i]);
          }

          local_list.clear();
          erase_list.clear();
          ady_tree.pop();
      }

      histogram.clear();

      ProbList tree_return;
      tree_return.prob.resize(map_max_+1);
      tree_return.prob = prob_tree;

      prob_tree.clear();

      return tree_return;
  }

  void printGraph(std::string gr_name, AdyList adyacences)
  {
      if (print==true)
      {
          char filename[128];

          FILE * pFile;
          //sprintf (filename, "%s/%s_%d.csv",path.c_str(),gr_name.c_str(),secs);
          sprintf (filename, "%s/%s.csv",path.c_str(),gr_name.c_str());
          pFile = fopen (filename,"w");
          fprintf (pFile,"Parent, Child\n");

          for (int n=0; n<adyacences.parent.size(); n++)
          {
              fprintf (pFile,"%d, %d\n",adyacences.parent[n],adyacences.child[n]);
          }

          fclose (pFile);
          ROS_INFO("Saved: %s/%s.csv",path.c_str(),gr_name.c_str());
      }
  }

  void printProbList(std::string prl_name, ProbList pr_list)
  {
      if (print==true)
      {
          char filename[128];

          FILE * pFile;
          //sprintf (filename, "%s/%s_%d.csv",path.c_str(),gr_name.c_str(),secs);
          sprintf (filename, "%s/%s.csv",path.c_str(),prl_name.c_str());
          pFile = fopen (filename,"w");
          fprintf (pFile,"Area, Probability\n");

          for (int n=0; n<pr_list.prob.size(); n++)
          {
              fprintf (pFile,"%d, %f\n",n,pr_list.prob[n]);
          }

          fclose (pFile);
          ROS_INFO("Saved: %s/%s.csv",path.c_str(),prl_name.c_str());
      }
  }

  void printFloatMap(std::string map_name, std::vector <double> p_map, int m_width)
  {
      if (print==true)
      {
          char filename[128];

          FILE * pFile;
          //sprintf (filename, "%s/%s_%d.csv",path.c_str(),gr_name.c_str(),secs);
          sprintf (filename, "%s/%s.csv",path.c_str(),map_name.c_str());
          pFile = fopen (filename,"w");
          int limit = 0;
          for (int n=0; n<p_map.size(); n++)
          {
              limit = n % m_width;
              if (n == 0)
              {
                  fprintf (pFile,"%6.2f",p_map[n]);
              }
              else if (limit != 0)
              {
              fprintf (pFile,", %6.2f",p_map[n]);
              }
              else
              {
                  fprintf (pFile,"\n%6.2f",p_map[n]);
              }
          }

          fclose (pFile);
          ROS_INFO("Saved: %s/%s.csv",path.c_str(),map_name.c_str());
      }
  }

  void printIntegerMap(std::string map_name, std::vector <int> p_map, int m_width)
  {
      if (print==true)
      {
          char filename[128];

          FILE * pFile;
          //sprintf (filename, "%s/%s_%d.csv",path.c_str(),gr_name.c_str(),secs);
          sprintf (filename, "%s/%s.csv",path.c_str(),map_name.c_str());
          pFile = fopen (filename,"w");
          int limit = 0;
          for (int n=0; n<p_map.size(); n++)
          {
              limit = n % m_width;
              if (n == 0)
              {
                  fprintf (pFile,"%d",p_map[n]);
              }
              else if (limit != 0)
              {
              fprintf (pFile,", %d",p_map[n]);
              }
              else
              {
                  fprintf (pFile,"\n%d",p_map[n]);
              }
          }

          fclose (pFile);
          ROS_INFO("Saved: %s/%s.csv",path.c_str(),map_name.c_str());
      }
  }


private:
  ros::NodeHandle nodeHandle;
  ros::Publisher im_pub2;
  ros::Subscriber sub1;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
    //Initiate ROS
    ros::init(argc, argv, "fm_interest");
    //Create an object of class SubscribeAndPublish that will take care of everything
    SubscribeAndPublish SAPObject;

    ros::Rate rate(0.3);
    srand(time(0)*time(0));

    ROS_INFO("Map Interest Node: Ok!!!");

    ros::spin();

    return 0;
}


