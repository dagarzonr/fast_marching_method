
/*=================================================================
% perform_fast_marching - launch the Fast Marching algorithm, in 2D or 3D.
%
%   [D,S,Q] = perform_fast_marching(W, start_points, options)
%
%   W is an (n,n) (for 2D, d=2) or (n,n,n) (for 3D, d=3)
%       weight matrix. The geodesics will follow regions where W is large.
%       W must be > 0.
%   'start_points' is a d x k array, start_points(:,i) is the ith starting point .
%
%   D is the distance function to the set of starting points.
%   S is the final state of the points : -1 for dead (ie the distance
%       has been computed), 0 for open (ie the distance is only a temporary
%       value), 1 for far (ie point not already computed). Distance function
%       for far points is Inf.
%   Q is the index of the closest point. Q is set to 0 for far points.
%       Q provide a Voronoi decomposition of the domain.
%
%   Optional:
%   - You can provide special conditions for stop in options :
%       'options.end_points' : stop when these points are reached
%       'options.nb_iter_max' : stop when a given number of iterations is
%          reached.
%   - You can provide an heuristic in options.heuristic (typically that try to guess the distance
%       that remains from a given node to a given target).
%       This is an array of same size as W.
%   - You can provide a map L=options.constraint_map that reduce the set of
%       explored points. Only points with current distance smaller than L
%       will be expanded. Set some entries of L to -Inf to avoid any
%       exploration of these points.
%   - options.values set the initial distance value for starting points
%   (default value is 0).
%
%   See also: perform_fast_marching_3d.
%
%   Copyright (c) 2007 Gabriel Peyre
*=================================================================*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/Pose.h"
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Path.h"
#include <tf/transform_broadcaster.h>
#include <stdlib.h>
#include <math.h>
#include <nav_msgs/Odometry.h>
#include <vector>
#include <string>
#include <time.h>  
#include <unistd.h>

// select to test or not to test (debug purpose)
// #define CHECK_HEAP check_heap(i,j,k);
#ifndef CHECK_HEAP
	#define CHECK_HEAP
#endif
// error display
// #define ERROR_MSG(a) mexErrMsgTxt(a)
#ifndef ERROR_MSG
	#define ERROR_MSG(a) 
#endif
// #define WARN_MSG(a)  mexWarnMsgTxt(a) 
#ifndef WARN_MSG
	#define WARN_MSG(a)
#endif

#include "perform_front_propagation_2d.h"
#include "fheap/fib.h"
#include "fheap/fibpriv.h"

#define kDead -1
#define kOpen 0
#define kFar 1

/* Global variables */
int n;			// width
int p;			// height
double* D = NULL;
double* S = NULL;
double* W = NULL;
double* Q = NULL;
double* start_points = NULL;
double* end_points = NULL;
double* values = NULL;
double* H = NULL;
double* L = NULL;
int nb_iter_max = 500000;
int nb_start_points = 0;
int nb_end_points = 0;
fibheap_el** heap_pool = NULL;

#define ACCESS_ARRAY(a,i,j) a[(i)+n*(j)]
#define D_(i,j) ACCESS_ARRAY(D,i,j)
#define S_(i,j) ACCESS_ARRAY(S,i,j)
#define W_(i,j) ACCESS_ARRAY(W,i,j)
#define H_(i,j) ACCESS_ARRAY(H,i,j)
#define Q_(i,j) ACCESS_ARRAY(Q,i,j)
#define L_(i,j) ACCESS_ARRAY(L,i,j)
#define heap_pool_(i,j) ACCESS_ARRAY(heap_pool,i,j)
#define start_points_(i,k) start_points[(i)+2*(k)]
#define end_points_(i,k) end_points[(i)+2*(k)]

struct point
{
	point( int ii, int jj )
	{ i = ii; j = jj; }
	int i,j;
};
typedef std::vector<point*> point_list;

inline 
bool end_points_reached(const int i, const int j )
{
	for( int k=0; k<nb_end_points; ++k )
	{
		if( i==((int)end_points_(0,k)) && j==((int)end_points_(1,k)) )
			return true;
	}
	return false;
}

inline 
int compare_points(void *x, void *y)
{
	point& a = *( (point*) x );
	point& b = *( (point*) y );
	if( H==NULL )
		return cmp( D_(a.i,a.j), D_(b.i,b.j) );
	else
		return cmp( D_(a.i,a.j)+H_(a.i,a.j), D_(b.i,b.j)+H_(b.i,b.j) );
}


// test the heap validity
void check_heap( int i, int j )
{
	for( int x=0; x<n; ++x )
		for( int y=0; y<p; ++y )
		{
			if( heap_pool_(x,y)!=NULL )
			{
				point& pt = * (point*)heap_pool_(x,y)->fhe_data;
				if( H==NULL )
				{
					if( D_(i,j)>D_(pt.i,pt.j) )
						ERROR_MSG("Problem with heap.\n");
				}
				else
				{
					if( D_(i,j)+H_(i,j)>D_(pt.i,pt.j)+H_(pt.i,pt.j) )
						ERROR_MSG("Problem with heap.\n");
				}
			}
		}
}


void perform_front_propagation_2d(T_callback_intert_node callback_insert_node)
{
	// create the Fibonacci heap
	struct fibheap* open_heap = fh_makeheap();
	fh_setcmp(open_heap, compare_points);

	double h = 1.0/n;
	
	// initialize points
	for( int i=0; i<n; ++i )
	for( int j=0; j<p; ++j )
	{
		D_(i,j) = GW_INFINITE;
		S_(i,j) = kFar;
		Q_(i,j) = -1;
	}

	// record all the points
	heap_pool = new fibheap_el*[n*p]; 
	memset( heap_pool, NULL, n*p*sizeof(fibheap_el*) );
	//memset( heap_pool, 0, n*p*sizeof(fibheap_el*) );		DAGR

	// inialize open list
	point_list existing_points;
	for( int k=0; k<nb_start_points; ++k )
	{
		int i = (int) start_points_(0,k);
		int j = (int) start_points_(1,k);

		if( D_( i,j )==0 )
			ERROR_MSG("start_points should not contain duplicates.");

		point* pt = new point( i,j );
		existing_points.push_back( pt );			// for deleting at the end
		heap_pool_(i,j) = fh_insert( open_heap, pt );			// add to heap
		if( values==NULL ) 
			D_( i,j ) = 0;
		else
			D_( i,j ) = values[k];
		S_( i,j ) = kOpen;
		Q_(i,j) = k;
	}

	// perform the front propagation
	int num_iter = 0;
	bool stop_iteration = GW_False;
	while( !fh_isempty(open_heap) && num_iter<nb_iter_max && !stop_iteration )
	{
		num_iter++;

		// current point
		point& cur_point = * ((point*) fh_extractmin( open_heap ));
		int i = cur_point.i;
		int j = cur_point.j;
		heap_pool_(i,j) = NULL;
		S_(i,j) = kDead;
		stop_iteration = end_points_reached(i,j);
		
		/*
		char msg[200];
		sprintf(msg, "Cool %f", Q_(i,j) );
		WARN_MSG( msg ); 
		*/
		
		CHECK_HEAP;

		// recurse on each neighbor
		int nei_i[4] = {i+1,i,i-1,i};
		int nei_j[4] = {j,j+1,j,j-1};
		for( int k=0; k<4; ++k )
		{
			int ii = nei_i[k];
			int jj = nei_j[k];
			bool bInsert = true;
			if( callback_insert_node!=NULL )
				bInsert = callback_insert_node(i,j,ii,jj);
			// check that the contraint distance map is ok
			if( ii>=0 && jj>=0 && ii<n && jj<p && bInsert )
			{
				double P = h/W_(ii,jj);
				// compute its neighboring values
				double a1 = GW_INFINITE;
				int k1 = -1;
				if( ii<n-1 )
				{
					bool bParticipate = true;
					if( callback_insert_node!=NULL )
						bParticipate = callback_insert_node(ii,jj,ii+1,jj);
					if( bParticipate )
					{
						a1 = D_(ii+1,jj);
						k1 = Q_(ii+1,jj);
					}
				}
				if( ii>0 )
				{
					bool bParticipate = true;
					if( callback_insert_node!=NULL )
						bParticipate = callback_insert_node(ii,jj,ii-1,jj);
					if( bParticipate )
					{
						if( D_(ii-1,jj)<a1 )
							k1 = Q_(ii-1,jj);
						a1 = GW_MIN( a1, D_(ii-1,jj) );
					}
				}
				double a2 = GW_INFINITE;
				int k2 = -1;
				if( jj<p-1 )
				{

					bool bParticipate = true;
					if( callback_insert_node!=NULL )
						bParticipate = callback_insert_node(ii,jj,ii,jj+1);
					if( bParticipate )
					{
						a2 = D_(ii,jj+1);
						k2 = Q_(ii,jj+1);
					}
				}
				if( jj>0 )
				{
					bool bParticipate = true;
					if( callback_insert_node!=NULL )
						bParticipate = callback_insert_node(ii,jj,ii,jj-1);
					if( bParticipate )
					{
						if( D_(ii,jj-1)<a2 )
							k2 = Q_(ii,jj-1);
						a2 = GW_MIN( a2, D_(ii,jj-1) );
					}
				}
				if( a1>a2 )	// swap so that a1<a2
				{
					double tmp = a1; a1 = a2; a2 = tmp;
					int tmpi = k1; k1 = k2; k2 = tmpi;
				}
				// update its distance
				// now the equation is   (a-a1)^2+(a-a2)^2 = P, with a >= a2 >= a1.
				double A1 = 0;
				if( P*P > (a2-a1)*(a2-a1) )
				{
					double delta = 2*P*P-(a2-a1)*(a2-a1);
					A1 = (a1+a2+sqrt(delta))/2.0;
				}
				else
					A1 = a1 + P;
				if( ((int) S_(ii,jj)) == kDead )
				{
					// check if action has change. Should not happen for FM
					// if( A1<D_(ii,jj) )
					//	WARN_MSG("The update is not monotone");
                    #if 1
					if( A1<D_(ii,jj) )	// should not happen for FM
					{
						D_(ii,jj) = A1;
						// update the value of the closest starting point
						//if( GW_ABS(a1-A1)<GW_ABS(a2-A1) && k1>=0  )
							Q_(ii,jj) = k1;
						//else
						//	Q_(ii,jj) = k2;
						//Q_(ii,jj) = Q_(i,j);
					}
                    #endif
				}
				else if( ((int) S_(ii,jj)) == kOpen )
				{
					// check if action has change.
					if( A1<D_(ii,jj) )
					{
						D_(ii,jj) = A1;
						// update the value of the closest starting point
						//if( GW_ABS(a1-A1)<GW_ABS(a2-A1) && k1>=0  )
							Q_(ii,jj) = k1;
						//else
						//	Q_(ii,jj) = k2;
						//Q_(ii,jj) = Q_(i,j);
						// Modify the value in the heap
						fibheap_el* cur_el = heap_pool_(ii,jj);
						if( cur_el!=NULL )
							fh_replacedata( open_heap, cur_el, cur_el->fhe_data );	// use same data for update
						else
							ERROR_MSG("Error in heap pool allocation."); 
					}
				}
				else if( ((int) S_(ii,jj)) == kFar )
				{
					if( D_(ii,jj)!=GW_INFINITE )
						ERROR_MSG("Distance must be initialized to Inf");
					if( L==NULL || A1<=L_(ii,jj) )
					{
						S_(ii,jj) = kOpen;
						// distance must have change.
						D_(ii,jj) = A1;
						// update the value of the closest starting point
						//if( GW_ABS(a1-A1)<GW_ABS(a2-A1) && k1>=0 )
							Q_(ii,jj) = k1;
						//else
						//	Q_(ii,jj) = k2;
						//Q_(ii,jj) = Q_(i,j);
						// add to open list
						point* pt = new point(ii,jj);
						existing_points.push_back( pt );
						heap_pool_(ii,jj) = fh_insert( open_heap, pt );			// add to heap	
					}
				}
				else 
					ERROR_MSG("Unkwnown state."); 
					
			}	// end switch
		}		// end for
	}			// end while
    //				char msg[200];
    //				sprintf(msg, "Cool %f", Q_(100,100) );
    //				 WARN_MSG( msg );

	// free heap
	fh_deleteheap(open_heap);
	// free point pool
	for( point_list::iterator it = existing_points.begin(); it!=existing_points.end(); ++it )
		GW_DELETE( *it );
	// free fibheap pool
	GW_DELETEARRAY(heap_pool);
}


class SubscribeAndPublish
{
public:

    struct fm_point {
        int x;
        int y;
        double t;
    };

    struct point_status {
        bool start;
        point_status() : start(false) {}
    } init, goal;

    struct map_status {
        bool state;
        map_status(): state(false){}
    }map_s;

    nav_msgs::OccupancyGrid map_in;
    geometry_msgs::PoseWithCovarianceStamped init_pose;
    geometry_msgs::PoseStamped goal_pose;

    double* out = NULL;
    std::vector <double> OccGrid;
    std::vector <double> fastMap;
    std::vector <signed char> fm_char;

    double neighborhood;

    double * MexFastMarching()
    {

      double target [2]={(init_pose.pose.pose.position.x - map_in.info.origin.position.x)/map_in.info.resolution ,(init_pose.pose.pose.position.y - map_in.info.origin.position.y)/map_in.info.resolution}; ////Target Position
        double *target_pr = &target[0];

        // first argument : weight list
        p = map_in.info.height;
        n = map_in.info.width;
        double *map_pr = &OccGrid[0];
        W = map_pr;

        // second argument : start_points
        start_points = target_pr;
        int tmp = 2;
        nb_start_points = 1;

        // third argument : end_points
        double finish[2]={19, 19}; //// Esto debe revisarse y cambiarse por el punto de salida -- ACOTAR LA BUSQUEDA!!!!!
        double *finish_pr = &finish[0];
        end_points = finish_pr;
        tmp = 2;
        nb_end_points = 0;

        //  argument 4: nb_iter_max
        nb_iter_max = 1000000;


        // first ouput : distance

        out = new double[n*p];   //// Esto debe revisarse y cambiarse por la entrada del mapa
        double *out_pr = out;
        D = out_pr;

        // second output : state

        S = new double[n*p];

        // third output : index

        Q = new double[n*p];

        // launch the propagation
        perform_front_propagation_2d();

            int size_out=n*p;

            fastMap.resize(size_out);

        for (int e=0; e<size_out; e++)				///// Esta es la condicion que debe cambiarse para enviar otro tipo de mapas diferentes a OccupancyGrid.
        {
            if (*(D+e)>=1e+09)
            {
                fastMap[e]=1e+09;  //  fastMap[e]=0;
            }
            else
            {
                fastMap[e] = *(D+e);
            }
        }

            GW_DELETEARRAY(S);

            GW_DELETEARRAY(Q);

            GW_DELETEARRAY(out);

            //GW_DELETEARRAY(D);  //Cannot be erased?

    return D;

    }

    std::vector <fm_point> fm_gradient(double neigh_param)
    {
        int x_m = (goal_pose.pose.position.x - map_in.info.origin.position.x)/map_in.info.resolution;
        int y_m = (goal_pose.pose.position.y - map_in.info.origin.position.y)/map_in.info.resolution;

        std::vector <fm_point> path;
        path.resize(1);

        int x_t = x_m;
        int y_t = y_m;

        int fm_size = fastMap.size();
        double fm_min = *std::min_element(&fastMap[0],&fastMap[0]+fm_size);
        double neigh_min;
        int p_vec = (y_m*map_in.info.width)+x_m;
        double ctrl = fastMap[p_vec];

        //ROS_INFO("fm_min - ctrl: %f, %f",fm_min, ctrl);

        int up;
        int down;
        int left;
        int right;

        int upright;
        int upleft;
        int downright;
        int downleft;

        std::vector <double> neigh_val;
        neigh_val.resize(8);
        int neigh_size;
        int count = 0;

        while (ctrl > fm_min)
        {
            path[count].x = x_t;
            path[count].y = y_t;
            path[count].t = ctrl;

            up = p_vec - map_in.info.width;  // TODO Protection against calling points outside the map: if (up<0 || up>fastMap.size())
            down = p_vec + map_in.info.width;
            left = p_vec - 1;
            right = p_vec + 1;

            upleft = up - 1;
            upright = up + 1;
            downleft = down - 1;
            downright = down + 1;

            neigh_val[0] = fastMap[up];
            neigh_val[1] = fastMap[down];
            neigh_val[2] = fastMap[left];
            neigh_val[3] = fastMap[right];

            neigh_val[4] = fastMap[upleft];
            neigh_val[5] = fastMap[upright];
            neigh_val[6] = fastMap[downleft];
            neigh_val[7] = fastMap[downright];

            if (x_t == 0)
            {
                neigh_val[2] = 1e+09;
            }
            if (x_t == map_in.info.width-1)
            {
                neigh_val[3] = 1e+09;
            }
            if (y_t == 0)
            {
                neigh_val[0] = 1e+09;
                neigh_val[4] = 1e+09;
                neigh_val[5] = 1e+09;
            }
            if (y_t == map_in.info.height-1)
            {
                neigh_val[1] = 1e+09;
                neigh_val[6] = 1e+09;
                neigh_val[7] = 1e+09;
            }

            if (neigh_param == 8.0)
            {
                neigh_size = neigh_val.size();
                neigh_min = *std::min_element(&neigh_val[0],&neigh_val[0]+neigh_size);

                if (neigh_min == neigh_val[0])
                {
                    y_t = y_t-1;
                }
                else if (neigh_min == neigh_val[1])
                {
                    y_t = y_t+1;
                }
                else if (neigh_min == neigh_val[2])
                {
                    x_t = x_t-1;
                }
                else if (neigh_min == neigh_val[3])
                {
                    x_t = x_t+1;
                }
                else if (neigh_min == neigh_val[4])
                {
                    y_t = y_t-1;
                    x_t = x_t-1;
                }
                else if (neigh_min == neigh_val[5])
                {
                    y_t = y_t-1;
                    x_t = x_t+1;
                }
                else if (neigh_min == neigh_val[6])
                {
                    y_t = y_t+1;
                    x_t = x_t-1;
                }
                else if (neigh_min == neigh_val[7])
                {
                    y_t = y_t+1;
                    x_t = x_t+1;
                }
                else
                {
                    ROS_INFO("Fast Marching: Neighborhood 8 Error!!!");
                }
            }
            else
            {
                neigh_size = neigh_val.size()-4;
                neigh_min = *std::min_element(&neigh_val[0],&neigh_val[0]+neigh_size);

                if (neigh_min == neigh_val[0])
                {
                    y_t = y_t-1;
                }
                else if (neigh_min == neigh_val[1])
                {
                    y_t = y_t+1;
                }
                else if (neigh_min == neigh_val[2])
                {
                    x_t = x_t-1;
                }
                else if (neigh_min == neigh_val[3])
                {
                    x_t = x_t+1;
                }
                else
                {
                    ROS_INFO("Fast Marching: Neighborhood 4 Error!!!");
                }
            }

            p_vec = (y_t*map_in.info.width)+x_t;
            ctrl = fastMap[p_vec];

            count = count + 1;
            path.resize(path.size()+1);
        }

        return path;
     }


    SubscribeAndPublish()
    {
        ros::NodeHandle nhParam("~");

        std::string init_pose_topic;
        std::string goal_pose_topic;
        std::string map_metric_topic;
        std::string map_fmm_topic;
        std::string plan_fmm_topic;

        nhParam.param("Init_Pose_topic", init_pose_topic, std::string("/Planner_Prediction/init_pose"));
        nhParam.param("Goal_Pose_topic", goal_pose_topic, std::string("/Planner_Prediction/goal_pose"));
        nhParam.param("Map_Metric_topic", map_metric_topic, std::string("/map_metric"));
        nhParam.param("Map_Fmm_topic", map_fmm_topic, std::string("/map_fm"));
        nhParam.param("Plan_Fmm_topic", plan_fmm_topic, std::string("/Plan_FM"));
        nhParam.param("Neighborhood", neighborhood, 8.0);

        //Topic you want to publish
        fm_pub1 = nodeHandle.advertise<std_msgs::Float32>("map_fm_max", 1);
        fm_pub2 = nodeHandle.advertise<nav_msgs::OccupancyGrid>(map_fmm_topic, 1);
        fm_pub3 = nodeHandle.advertise<nav_msgs::Path>(plan_fmm_topic, 1);

        //Topic you want to subscribe
        sub1 = nodeHandle.subscribe(map_metric_topic, 1, &SubscribeAndPublish::Mapcallback,this);
        sub2 = nodeHandle.subscribe(init_pose_topic, 1, &SubscribeAndPublish::Initcallback,this);
        sub3 = nodeHandle.subscribe(goal_pose_topic, 1, &SubscribeAndPublish::Goalcallback,this);
    }

    // ROS Callbacks


    void Mapcallback(const nav_msgs::OccupancyGrid::ConstPtr& map_msg)
    {
        map_in.header = map_msg->header;
        map_in.info = map_msg->info;
        map_s.state = true;

        int size_of = map_in.info.height*map_in.info.width;
        OccGrid.resize(size_of);
        int aux;

        for (int a=0; a<size_of; a++)
        {
            aux = (int)map_msg->data[a];
            OccGrid[a] = (double)aux;
        }

        return;
    }

	void Initcallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& init_msg)
	{
        if (map_s.state == true)
		{
            nav_msgs::OccupancyGrid map_fm;
            std_msgs::Float32 map_fm_max;

            init_pose.header = init_msg->header;
            init_pose.pose = init_msg->pose;

            map_fm.header.stamp = init_pose.header.stamp;
            map_fm.header.frame_id = map_in.header.frame_id;
            map_fm.info = map_in.info;

			init.start = true;
            ROS_INFO("Fast Marching - Init = %f %f %f",init_pose.pose.pose.position.x, init_pose.pose.pose.position.y, init_pose.pose.pose.position.z);

			MexFastMarching();	

			int fm_size = fastMap.size();
			double fm_max = 0;

			for (int w=0; w<fm_size; w++) 
			{
				if (fastMap[w] < 1e+09 && fastMap[w] > fm_max) 
				{
					fm_max = fastMap[w];
				}
			}

			fm_char.resize(fm_size);

			for (int z=0; z<fm_size; z++)				///// Esta es la condicion que debe cambiarse para enviar otro tipo de mapas diferentes a OccupancyGrid.
			{	
				if (fastMap[z]>=1e+09){
					fm_char[z]=(signed char)100;
				}
				else {
                    fm_char[z]=(signed char)round(99*(fastMap[z]/fm_max));
				}
			}

			map_fm_max.data = fm_max;
			map_fm.data = fm_char;
	
			fm_pub1.publish(map_fm_max);
			fm_pub2.publish(map_fm);

			fm_char.clear();

			// Re-planning
			if (goal.start == true)
			{
				std::vector <fm_point> path_grad;
		 		nav_msgs::Path path_fm;

                path_grad = fm_gradient(neighborhood);
				int pgrad_size = path_grad.size();
				path_fm.poses.resize(pgrad_size-1);

                path_fm.header.stamp = init_pose.header.stamp;
                path_fm.header.frame_id = map_in.header.frame_id;

				for(int p = 0; p < pgrad_size-1; p++)
				{
                    path_fm.poses[p].header.stamp = init_pose.header.stamp;
                    path_fm.poses[p].header.frame_id = map_in.header.frame_id;
                    path_fm.poses[p].pose.position.x = map_in.info.origin.position.x + (path_grad[p].x + 0.5)*map_in.info.resolution;
                    path_fm.poses[p].pose.position.y = map_in.info.origin.position.y + (path_grad[p].y+ 0.5)*map_in.info.resolution;  // Falta añadir headers y fm_t
					path_fm.poses[p].pose.position.z = 0;
				}
				fm_pub3.publish(path_fm);
			}
		}
		else
		{
			ROS_INFO("Fast Marching: You need a map before a start point!!!");
		}
	}

	void Goalcallback(const geometry_msgs::PoseStamped::ConstPtr& goal_msg)
	{
		if (init.start == true)
		{
            goal_pose.header = goal_msg->header;
            goal_pose.pose = goal_msg->pose;

			goal.start = true;
            ROS_INFO("Fast Marching - Goal = %f %f %f",goal_pose.pose.position.x, goal_pose.pose.position.x, goal_pose.pose.position.z);

			std::vector <fm_point> path_grad;
	 		nav_msgs::Path path_fm;

			path_grad = fm_gradient(neighborhood);
			int pgrad_size = path_grad.size();
			path_fm.poses.resize(pgrad_size-1);

            path_fm.header.stamp = goal_pose.header.stamp;
            path_fm.header.frame_id = map_in.header.frame_id;

			for(int p = 0; p < pgrad_size-1; p++)
			{              
                path_fm.poses[p].header.stamp = goal_pose.header.stamp;
                path_fm.poses[p].header.frame_id = map_in.header.frame_id;
                path_fm.poses[p].pose.position.x = map_in.info.origin.position.x + (path_grad[p].x + 0.5)*map_in.info.resolution;
                path_fm.poses[p].pose.position.y = map_in.info.origin.position.y + (path_grad[p].y + 0.5)*map_in.info.resolution;  // Falta añadir headers y fm_t
				path_fm.poses[p].pose.position.z = 0;
			}

			fm_pub3.publish(path_fm);
		}
		else
		{
			ROS_INFO("Fast Marching: You need a start point before a goal!!!");
		}
	}

private:

    ros::NodeHandle nodeHandle;
    ros::Publisher fm_pub1;
    ros::Publisher fm_pub2;
    ros::Publisher fm_pub3;
    ros::Subscriber sub1;
    ros::Subscriber sub2;
    ros::Subscriber sub3;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
    //Initiate ROS
	ros::init(argc, argv, "fast_marching");

    //Create an object of class SubscribeAndPublish that will take care of everything
    SubscribeAndPublish SAPObject;

	ros::Rate rate(0.3);
	srand(time(0)*time(0));

	ROS_INFO("Fast Marching Node: Ok!!!");
    ROS_INFO("Fast Marching Node: Waiting for Init and Goal");
    ros::spin();

    return 0;
}
