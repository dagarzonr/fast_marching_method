#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/Pose.h"
#include "nav_msgs/OccupancyGrid.h"
#include <tf/transform_broadcaster.h>
#include <stdlib.h>
#include <math.h>
#include <nav_msgs/Odometry.h>
#include <vector>
#include <string>
#include <time.h>  

class SubscribeAndPublish
{
public:

    struct map_data
    {
        std::vector <signed char> data;
    }map_out;

    SubscribeAndPublish()
    {
        std::string map_metric_topic;
        std::string map_cost_topic;
        ros::NodeHandle nhParam("~");

        nhParam.param("Map_Metric_topic", map_metric_topic, std::string("/map_metric"));
        nhParam.param("Map_Cost_topic", map_cost_topic, std::string("/Global_Cost"));
        //Topic you want to publish
        fm_pub2 = nodeHandle.advertise<nav_msgs::OccupancyGrid>(map_metric_topic, 1);

        //Topic you want to subscribe
        sub1 = nodeHandle.subscribe(map_cost_topic, 1, &SubscribeAndPublish::Mapcallback, this);
    }

    void Mapcallback(const nav_msgs::OccupancyGrid::ConstPtr& map_msg)
    {

        nav_msgs::OccupancyGrid map_metric;

        map_metric.info = map_msg->info;
        map_metric.header = map_msg->header;

        int map_size = map_msg->data.size();

        map_out.data.resize(map_size);

        for (int a=0; a<map_size; a++)				///// This should be changed to use other metrics
        {
            //	if (tmp_data[a]==-1 || tmp_data[a]==100)		// Solo como guia... el grid viene como signed char de -1, 0 y 100
            if ((int)map_msg->data[a] >= 0 && (int)map_msg->data[a] < 100)
            {
                map_out.data[a] = (signed char)100-map_msg->data[a];
            }
            else
            {
                map_out.data[a] = (signed char)0;
            }

        }

        map_metric.data = map_out.data;
        fm_pub2.publish(map_metric);

        map_out.data.clear();

        return;
    }

private:
    ros::NodeHandle nodeHandle;
    ros::Publisher fm_pub2;
    ros::Subscriber sub1;

};//End of class SubscribeAndPublish

int main(int argc, char **argv)
{
    //Initiate ROS
    ros::init(argc, argv, "fm_metric");
    //Create an object of class SubscribeAndPublish that will take care of everything
    SubscribeAndPublish SAPObject;

    ros::Rate rate(0.3);
    srand(time(0)*time(0));

	ROS_INFO("Map Metric Node: Ok!!!");

    ros::spin();

    return 0;
}
